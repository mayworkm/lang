


int_list="çok güzel yemekleri yiyeceğim bugün!"

user_string = int_list
simplified_string=''
romanize_dict = {'ı':'i','İ':'I','ğ':'g','ç':'c','ş':'s','ö':'o','ü':'u'}

for char in user_string:
    if char not in " ',.!?;:":
        if char in romanize_dict:
            simplified_string += romanize_dict[char]
        else:
            simplified_string += char

print (simplified_string)