import datetime
from django.utils import timezone
from django.test import TestCase, RequestFactory
from django.contrib.auth.models import AnonymousUser, User
from django.urls import reverse
from .models import Attempt, Comment, Course, Entry, GramConcept, GrammarProgress, Grammar, Language, Question, Sentence, Stage, StudentProgress, Student

class ViewTests(TestCase):
    pass

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='joe',email='joe@hotmail.com', password='qweqweqwe')
        self.u2 = User.objects.create_user(username='max',email='max@hotmail.com', password='qweqweqwe')
        self.u3 = User.objects.create_user(username='sally',email='sally@hotmail.com', password='qweqweqwe')
        self.l_EN = Language.objects.create(anglicized_name="English",name="English",author=self.user)
        self.l_TR = Language.objects.create(anglicized_name="Turkish",name="Türkçe",author=self.user)
        self.l_RU = Language.objects.create(anglicized_name="Russian",name="русский",author=self.user)
        self.st1 = Student.objects.create(user=self.user)
        self.st1.native_languages.add(self.l_EN)
        self.c1 = Course.objects.create(title='Learn Turkish',native_language=self.l_EN,target_language=self.l_TR,author=self.user)
        self.c2 = Course.objects.create(title='Speak like a Russian',native_language=self.l_EN,target_language=self.l_RU,author=self.user)
        self.c3 = Course.objects.create(title='Rusça öğren',native_language=self.l_TR,target_language=self.l_RU,author=self.user)
        self.gc1 = GramConcept.objects.create(title='Plurals',author=self.user)
        self.g1 = Grammar.objects.create(gram_concept=self.gc1,target_language=self.l_TR,native_language=self.l_EN,score=1000,author=self.user)
        self.sen1 = Sentence.objects.create(text="Books",language=self.l_EN,author=self.user)
        self.sen2 = Sentence.objects.create(text="Kitaplar",language=self.l_TR,author=self.user)
        self.sen2.grammars.add(self.g1)
        self.q1 = Question.objects.create(front=self.sen1,back=self.sen2,grammar=self.g1,author=self.user)
        for x in range(6):
            # create 6 random extra questions
            Question.objects.create(front=self.sen1,back=self.sen2,grammar=self.g1,author=self.user)
        self.q2 = Question.objects.create(front=self.sen1,back=self.sen2,grammar=self.g1,author=self.user)
        self.q2 = Question.objects.create(front=self.sen1,back=self.sen2,grammar=self.g1,author=self.user)
        self.q2 = Question.objects.create(front=self.sen1,back=self.sen2,grammar=self.g1,author=self.user)
                        
        self.stage1 = Stage.objects.create(position=1,title="One",course=self.c1,author=self.user) 
        self.stage2 = Stage.objects.create(position=2,title="Two",course=self.c1,author=self.user) 
        self.e1 = Entry.objects.create(text="booklar",simplified="booklar",question=self.q1)
        self.a1 = Attempt.objects.create(user=self.user,entry=self.e1,assessment=Attempt.WRONG)
        self.a2 = Attempt.objects.create(user=self.user,entry=self.e1,assessment=Attempt.WRONG)
        self.a3 = Attempt.objects.create(user=self.u2,entry=self.e1,assessment=Attempt.CORRECT)
        self.a4 = Attempt.objects.create(user=self.u3,entry=self.e1,assessment=Attempt.REPORT,feedback="I think booklar is also right")
        return self
    
    def test_NoRegistrations_to_Registered_for_Course(self):
        self.client.login(username='joe', password='qweqweqwe')

        # correct dashboard output
        response = self.client.get(reverse('Quiz:dashboard'))
        self.assertRedirects(response,reverse('Quiz:signup-course'))
        # subscribing, shows correct list to choose from
        response = self.client.get(reverse('Quiz:signup-course'))
        self.assertContains(response, self.c1.title)
        self.assertContains(response, self.c2.title)
        self.assertNotContains(response, self.c3.title)
        # sign up for Learn Turkish
        response = self.client.post(reverse('Quiz:register-student-to-course'), {'choice': self.c1.id})
#        self.assertRedirects(response,reverse('Quiz:initial_placement_init'))
        self.sp = StudentProgress.objects.first()
        # simulate assessment
        self.sp.score = 3000
        self.sp.variance = StudentProgress.VARIANCE_CUTOFF_INITIAL_PLACEMENT
        self.sp.initial_placement = False 
        self.sp.save()
        # after assessment
        # subscribed - dashboard shows correct output
        response = self.client.get(reverse('Quiz:dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.c1.title)
        # attempt sign-up, confirm Learn Turkish is no longer shown
        response = self.client.get(reverse('Quiz:signup-course'))
        self.assertNotContains(response, self.c1.title)
        self.assertContains(response, self.c2.title)
        self.assertNotContains(response, self.c3.title)



    def test_Weekly_Assessment(self):
        self.test_NoRegistrations_to_Registered_for_Course()
        # start weekly assessment
        response = self.client.get(reverse('Quiz:assessment_init'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('Quiz:assessment_schedule_qs'))
#        while (response.url == reverse('Quiz:assessment_schedule_qs')):
#            response = self.client.get(response.url)
#        self.assertEqual(response.url, reverse('Quiz:assessment_ask_qs'))
        
        # confirm a question is posed

class AttemptTests(TestCase):
    pass
    def test_hardest_questions(self):
        # returns questions with the greatest percentage of Wrong self_assessments.
        pass
        
    
    def test_most_ambiguous_questions(self):
        # returns questions with the greatest percentage of Ambiguous self_assessments.
        pass
 
class CommentTests(TestCase):
    pass

class CourseTests(TestCase):
    pass
   
class EntryTests(TestCase):

    def setUp(self):
        t = ViewTests.setUp(self)
        print (self)
        
    def test_unique_users_several(self):        
        self.assertEqual(self.e1.unique_users(), 3)
        
    def test_unique_users_none(self):
        pass

    def test_recurring_patterns(self):
        pass
    
class GramConceptTests(TestCase):
    pass


class GrammarProgressTests(TestCase):
    pass

class GrammarTests(TestCase):
    pass

class LanguageTests(TestCase):
    pass

class QuestionTests(TestCase):
    pass

class SentenceTests(TestCase):
    pass

class StageTests(TestCase):
    pass
 
class StudentProgressTests(TestCase):
    pass
    
class StudentTests(TestCase):
    pass

