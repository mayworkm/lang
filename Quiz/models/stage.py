from django.db import models
from django.contrib.auth.models import User
import logging

# Stages can provide an ordering to grammars, as well as milestones to mark a student's progress. 

class StageManager(models.Manager):
#     def get_queryset(self):
#         return StageQuerySet(self.model, using=self._db)
    pass


class Stage(models.Model):
    position = models.IntegerField()
    titles = models.ManyToManyField('Sentence',blank=True)
    course = models.ForeignKey('Course')
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    last_edited = models.DateField(auto_now=True)
    # to access stage's grammars, stage.grammar_set
    objects = StageManager()

    def __str__(self):
        if self.titles.exists():
            title = self.titles.order_by('id').first().text
        else:
            title = ""        
        return str(str(self.position) + ". " + title)

    def title_in(self,language):
        title = self.titles.filter(language=language).first()
        if title is not None:
            title = title.text
        else:
            title = ""        
        return str(str(self.position) + ". " + title)

    def __init__(self, *args, **kwargs):
        super(Stage, self).__init__(*args, **kwargs)

    def score(self):
        return self.position*1000

    def save(self, *args, **kwargs):
        if self.score() != self.score:
            old_score = self.score()
            self.score = self.score()
            for grammar in self.grammar_set.all():
                # modify grammar scoreswork out average of scores resave score with offset.
                offset = grammar.score - old_score
                grammar.score = self.score + offset
                grammar.save()
        super(Stage, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model Stage Instance saved:"+str(self))
        
    class Meta:
        ordering = ['position'] # lowest scores first
        unique_together = ("position", "course")
