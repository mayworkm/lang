from django.db import models
from django.contrib.auth.models import User
import logging

# Languages are used to populate native_language and target_language fields
# Each language has a name, written in the script of that language, and an
# anglicized_name expressing the English name for that language.

class Language(models.Model):
    name = models.CharField(max_length=200, unique=True)
    anglicized_name = models.CharField(max_length=200, unique=True)
    iso693_2 = models.CharField(max_length=8, help_text="Please find the correct code at: https://www.loc.gov/standards/iso639-2/php/code_list.php") 
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return (self.anglicized_name)

    def save(self, *args, **kwargs):
        super(Language, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model Language Instance saved:"+str(self))

    class Meta:
        ordering = ['anglicized_name']
