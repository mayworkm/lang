from django.db import models
from django.contrib.auth.models import User
import logging

# A Grammar Concept gives the abstract linguistical name to grammars as implemented in
# different languages. Future tense is expressed in varying grammars in different languages
# but future tense as a gramattical concept will exist in most.
# Grammar concepts help tie sentences of different languages together as related.

class GramConcept(models.Model):
    title = models.CharField(max_length=200, help_text="Linguistical name")
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    last_edited = models.DateField(auto_now=True)
    
    def __str__(self):
        return str(self.title)

    def save(self, *args, **kwargs):
        super(GramConcept, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model GramConcept Instance saved:"+str(self))
        