from django.db import models
from django.contrib.auth.models import User
from .course import Course
import logging

# Student is an extension of the User model. It records a user's native_language and the 
# course(s) they are enrolled in.
# many methods allow quick access to relevant information about the student.
# context for these informations (for example which course they are currently examining)
# is determined by the values stored in the current session variables.

class Student(models.Model):
    user = models.OneToOneField(User, unique=True, on_delete=models.CASCADE)
    native_languages = models.ManyToManyField('Language', related_name='native_students') #forbids deletion of a language while users still have it set
    hours_per_day = models.FloatField(default=1,help_text="From student reports, an average of what study the student is putting in")
    
    def target_languages(self):
        return course.studentprogress_set.filter(user=self.user).values_list('target_language', flat=True)

    def save(self, *args, **kwargs):
        super(Student, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model Student Instance saved:"+str(self))
    
    def __str__(self):
        return self.user.username
    