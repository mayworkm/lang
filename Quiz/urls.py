from django.conf.urls import include,url
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views
from . import views

#from .views import GramConceptList, GramConceptCreate, GramConceptUpdate, GrammarUpdate

app_name = 'Quiz'

urlpatterns = [
    url('^', include('django.contrib.auth.urls')),
    url(r'^$', views.dashboard, name='dashboard'),   
    url(r'^gramconcept/$', views.GramConceptList.as_view(), name='gramconcept-list'),
    url(r'^gramconcept/create/$', views.GramConceptCreate.as_view(), name='gramconcept-create'),
    url(r'^gramconcept/update/(?P<pk>[0-9]+)/$', views.GramConceptUpdate.as_view(), name='gramconcept-update'),
    url(r'^gramconcept/delete/(?P<pk>[0-9]+)/$', views.GramConceptDelete.as_view(), name='gramconcept-delete'),
    url(r'^grammar/$', views.GrammarList.as_view(), name='grammar-list'),
    url(r'^grammar/create/$', views.GrammarCreate.as_view(), name='grammar-create'),
    url(r'^grammar/update/(?P<pk>[0-9]+)/$', views.GrammarUpdate.as_view(), name='grammar-update'),
    url(r'^grammar/delete/(?P<pk>[0-9]+)/$', views.GrammarDelete.as_view(), name='grammar-delete'),
    url(r'^prep_select_course/$', views.prep_select_course, name='prep_select_course'),
    url(r'^select_course/$', views.select_course, name='select-course'),
    url(r'^signup_course/$', views.signup_course, name='signup-course'),
    url(r'^register_student_to_course/$', views.register_student_to_course, name='register-student-to-course'),
    url(r'^reset_all_student_progress/$', views.reset_all, name='reset-all'),
    url(r'^new_question/(?P<stage_id>[0-9]+)/$', views.new_question, name='new-question'),
    url(r'^new_question_from_sentence/(?P<sentence_id>[0-9]+)/$', views.new_question_from_sentence, name='new_question_from_sentence'),
    url(r'^new_question/$', views.new_question, name='new-question'),    
    url(r'^gram_create/$', views.gram_create, name='gram-create'), 
    url(r'^gram_update/(?P<id>[0-9]+)/$', views.gram_update, name='gram-update'), 
    url(r'^grammar-restage/(?P<gram_id>[0-9]+)/(?P<active_tab>[a-z]+)/$', views.grammar_restage, name='grammar-restage'),     
    url(r'^initial_placement_init/$', views.initial_placement_init, name='initial_placement_init'),
    url(r'^run_initial_placement/$', views.run_initial_placement, name='run_initial_placement'),    
    url(r'^reset_all/$', views.reset_all, name='reset-all'),    
    url(r'^integrity_check/$', views.integrity_check, name='integrity_check'),    
    url(r'^integrity_check/(?P<course_id>[0-9]+)/$', views.integrity_check, name='integrity_check'),    
    url(r'^assessment_init/$', views.assessment_init, name='assessment_init'), 
    url(r'^assessment_schedule_qs/$', views.assessment_schedule_qs, name='assessment_schedule_qs'),
    url(r'^assessment_report/$', views.assessment_report, name='assessment_report'),
    url(r'^assessment_ask_qs/$', views.assessment_ask_qs, name='assessment_ask_qs'),
    url(r'^question_score/$', views.question_score, name='question-score'),
    url(r'^ajax/submit_answer/$', views.ajax_submit_answer, name='ajax_submit_answer'),
    url(r'^add_stage/$', views.add_stage, name='add_stage'),
    url(r'^example_questions/(?P<grammar_id>[0-9]+)/$', views.example_questions, name='example_questions'), 
    
    
]
# 
# 
# urlpatterns = [ # ... 
#    url( r'author/ add/ $', AuthorCreate.as_view(), name =' author_add'), 
#    url( r'author/(? P < pk >[ 0-9] +)/ $', AuthorUpdate.as_view(), name =' author_update'), 
#    url( r'author/(? P < pk >[ 0-9] +)/ delete/ $', AuthorDelete.as_view(), name =' author_delete'),


#     # ex: /poll/2/
#     url(r'^(?P<topic_id>[0-9]+)/$', views.tally, name='tally'),
#     # ex: /poll/detail/e23d2q45/
#     url(r'^detail/(?P<token_id>[a-z0-9]+)/$', views.detail, name='detail'),
#     # Ex: /poll/vote/    because of csrf tokens, may not need any more than "vote"
#     url(r'^vote/$', views.vote, name='vote'),
#     # Ex: /poll/newtopic/
#     url(r'^topicf/$', views.CreateTopic.as_view(), name='CreateTopic'),
