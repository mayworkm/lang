from django import forms
from .models import Question, Grammar, Course, GramConcept, Language
from django.forms import ModelMultipleChoiceField
from django.contrib import messages

# from crispy_forms.helper import FormHelper
# from crispy_forms.layout import * # adding a submit button
# from crispy_forms.utils import render_crispy_form
# from crispy_forms.bootstrap import StrictButton

class UpdateGrammarForm(forms.Form):
    title = forms.CharField(max_length=100)
    explanation = forms.CharField(required=False,widget=forms.Textarea)
    gram_concept = forms.ModelMultipleChoiceField(queryset=GramConcept.objects.all())  
    target_language = forms.ModelMultipleChoiceField(queryset=Language.objects.all())
    native_language = forms.ModelMultipleChoiceField(queryset=Language.objects.all(), help_text="the native language for which this is providing a translation")
    score = forms.IntegerField(required=False)

    def __init__(self, *args, **kwargs): 
#        g_id = kwargs.pop('id')
        super(UpdateGrammarForm, self).__init__(*args, **kwargs) 
        example_question_fld = forms.ModelMultipleChoiceField(
            queryset = Question.objects.filter(grammar=self,
                                               target_language=course.target_language,
                                               native_language=sp.native_language,
                                              ))
        self.fields['example_question'] = example_question_fld


class CreateGrammarForm(forms.Form):
    Title = forms.CharField(max_length=100)
    Explanation = forms.CharField(required=False)
    GramConcept = forms.ModelMultipleChoiceField(queryset=GramConcept.objects.all())   

    def __init__(self, *args, **kwargs): 
        course = kwargs.pop('course')
        super(CreateGrammarForm, self).__init__(*args, **kwargs) 
#         example_choices = Question.objects.filter(grammar=self)
#         e_fld = ModelMultipleChoiceField(queryset=example_choices)        
#         self.fields['example_question'] = e_fld

class CreateQuestionForm(forms.Form):
    NativeSentence = forms.CharField(max_length=200, label='Native language sentence')
    TargetSentence = forms.CharField(max_length=200, label='Target language sentence')
    Hint = forms.CharField(required=False, help_text="Written in the native language, a hint that directs a user of this language towards selecting the correct answer. Useful where the target language may allow multiple equivalents")
    Explanation = forms.CharField(required=False, help_text="Written in the native language, an explanation to peculiarities in the answer")

    def __init__(self, *args, **kwargs): 
        course = kwargs.pop('course')
        super(CreateQuestionForm, self).__init__(*args, **kwargs) 
        grammar_choices = Grammar.objects.filter(stage__course=course)
        self.fields['GrammarsIncluded[]'] = ModelMultipleChoiceField(queryset=grammar_choices)
        self.fields['GrammarsIncluded[]'].label = 'Featured Grammars'
        stage_choices = course.stage_set.all()        
        self.fields['stages[]'] = ModelMultipleChoiceField(
                                queryset=stage_choices,required=False)
