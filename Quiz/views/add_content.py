from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView 
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from ..models import * #GramConcept, Stage, Grammar, Student, StudentProgress, Course, Question, GrammarProgress
from django.http.response import Http404
from django.contrib import messages
from ..forms import * #CreateQuestionForm, CreateGrammarForm
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
import logging
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied

@login_required
def gram_update(request, id):
    # This is a good example of how to update an existing object without using a Modelview
    instance = get_object_or_404(Grammar, id=id)
    form = UpdateGrammarForm (request.POST or None,instance=instance)
    if form.is_valid():
        form.save()   
        # create a fresh form for the next entry
        messages.add_message(request, messages.SUCCESS, 'Grammar successfully saved')
        form = CreateGrammarForm (course=course)
        log = logging.getLogger(__name__)
        log.info("User" + request.user.username + "updated grammar "+ str(instance))
 
@login_required
def gram_create(request):
    course = get_object_or_404(Course, id = request.session['course_pk'])
    sp = StudentProgress.objects.get(id=request.session['sp_pk'])
    if request.POST:
        form = CreateGrammarForm (request.POST,course=course)
        request.session['active_tab'] = "create" 
    else:
        form = CreateGrammarForm (course=course)

    if form.is_valid():
        # create title entry
        new_title = Sentence.objects.create(
            text = form.cleaned_data.get("Title"),
            language = sp.native_language,
            author = request.user,            
            )
        
        # create explanation entry
        new_expl = Comment.objects.create(
            text = form.cleaned_data.get("Explanation"),
            language = sp.native_language,
            author = request.user,
            ) 
        
        # create grammar entry
        new_gram = Grammar.objects.create(
            gram_concept = form.cleaned_data.get("GramConcept").first(),
            target_language = course.target_language,
            author=request.user,            
        )
        # add title and explanation
        new_gram.titles.add(new_title)
        new_gram.explanations.add(new_expl)
        log = logging.getLogger(__name__)
        log.info("User" + request.user.username + "created grammar "+ str(new_gram))
        
        # create a fresh form for the next entry
        messages.add_message(request, messages.SUCCESS, 'Grammar successfully saved')
        form = CreateGrammarForm (course=course)
        request.session['active_tab'] = "create"



    # Populates existing grammars assigned to stages of current course
    stages_raw = course.stage_set.all()
    try:
        active_tab = request.session['active_tab'] 
    except:
        active_tab = "existing"
    countlist = {}
    stage_titles = {}
    stage_ids = {}
    grammar_titles = {}
    grammar_concepts = {}
    grammar_explanation = {}
    grammar_example_question = {}
    grammar_id = {}
    gram_stage = {}
    questions = {}
    stages = {}
    counter = 0
    for stg in stages_raw:
        countlist[counter] = "stage"
        stage_titles[counter] = stg.title_in(sp.native_language)
        stage_ids[counter] = stg.id
        stages[counter] = {'title':stg.title_in(sp.native_language),
                           'id':stg.id
                           }
        last_stage_title = stg.title_in(sp.native_language)
        counter += 1

        relevant_grammars = stg.grammar_set.all().order_by('score')
        for grm in relevant_grammars:
            # mark those without example sentence, explanation, score. dropdown to assign grammar to new stage</p>
            countlist[counter] = "grammar"
            grammar_titles[counter] = grm.title_in(sp.native_language)
            grammar_concepts[counter] = grm.gram_concept.title
            grammar_id[counter] = grm.id
            questions[counter] = Question.objects.filter(grammar=grm.id).count()
            stage_titles[counter] = last_stage_title
            grammar_explanation[counter]= (grm.explanation_in(sp.native_language) != "None")
            grammar_example_question[counter]= grm.example_questions.filter(grammar=grm,
                                                                            target_language=course.target_language,
                                                                            native_language=sp.native_language
                                                                            ).exists()    
            counter += 1


    # grammars unassigned to a stage of this course
    unassigned_grammars_raw = Grammar.objects.filter(target_language=course.target_language).exclude(stage__course=course)
    unassigned_grammars={}
    for x,u_grm in enumerate(unassigned_grammars_raw):
        unassigned_grammars[x] = {'id':u_grm.id,
                                  'title':u_grm.title_in(sp.native_language),
                                  'gc_title':u_grm.gram_concept.title
                                  }
    # uninstantiated concepts
    inst_gram_concepts_id = Grammar.objects.filter(target_language=course.target_language).values_list('gram_concept', flat=True)
    inst_gram_concepts = GramConcept.objects.filter(id__in= inst_gram_concepts_id)
    uninstantiated_gram_concepts = GramConcept.objects.exclude(id__in= inst_gram_concepts_id)


    context = {
        'active_tab': active_tab,
        'form': form,
        'course': course, 
        'stages': stages,
        'countlist': countlist,
        'stage_titles': stage_titles,
        'stage_ids': stage_ids,
        'grammar_titles': grammar_titles,
        'grammar_concepts': grammar_concepts,
        'grammar_explanation': grammar_explanation,
        'grammar_example_question': grammar_example_question,
        'grammar_id': grammar_id,
        'questions': questions,
        'unassigned_grammars': unassigned_grammars,
        'uninstantiated_gram_concepts': uninstantiated_gram_concepts,
        'inst_gram_concepts': inst_gram_concepts,
    }
    return render(request, 'Quiz/gram_create.html', context)

@login_required
def add_stage(request):
    course = get_object_or_404(Course, id = request.session['course_pk'])    
    last_pos = course.stage_set.order_by("position").last().position
    new_stage = Stage.objects.create(position=last_pos+1,course=course,author=request.user)
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + "added stage "+ str(new_stage))
    return redirect('Quiz:gram-create')

def check_perms_or_403(request,perm,message):
    from django.contrib.auth.models import Permission  
    app_label,codename=perm.split('.')  
    if not request.user.has_perms(perm):
        permO = Permission.objects.get(codename=codename)
        raise PermissionDenied(permO.name)
        return False
    return True

@permission_required('quiz.restage_grammar',raise_exception=True)
def grammar_restage(request, gram_id, active_tab):
#    check_perms_or_403 (request,'quiz.restage_grammar','Content creation')
    stage = get_object_or_404(Stage, id = request.POST['stage'])
    grammar = get_object_or_404(Grammar, id = gram_id)
    course = stage.course

    # confirm grammar and stage are appropriately matched
    if course.target_language != grammar.target_language:
        raise Http404("This grammar is for a different language to the Stage's course")

    # reset grammar to new stage
    grammar.stage = stage
    grammar.save()
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + "restaged grammar "+ str(grammar)+" to stage "+str(stage))

    # redirect with active_tab set
    request.session['active_tab'] = active_tab
    return redirect('Quiz:gram-create')
    
    
@login_required
def new_question(request):
    
    sp = StudentProgress.objects.get(id=request.session['sp_pk'])
    course = get_object_or_404(Course, id = request.session['course_pk'])
    all_stages = []
    for stg in course.stage_set.all():
        grammars = []
        for grammar in stg.grammar_set.all():
            grammars.append({'title':grammar.full_title(sp.native_language),'id':grammar.id})
        all_stages.append({'title':stg.title_in(sp.native_language),'id':stg.id,'grammars':grammars})
    if 'NativeFront' in request.POST:
        NativeFront = True
    else:
        NativeFront = False
    form = CreateQuestionForm ((request.POST or None),course=course)
    # remember selected stages
    selected_stages = []
    try:
        for stage in request.POST.getlist("selected_stages[]"):
            selected_stages.append(int(stage))        
    except:
        pass

    if form.is_valid():
        GrammarsIncluded = form.cleaned_data.get("GrammarsIncluded[]")
        # create sentence objects
        native_sentence = Sentence.objects.create(
            text = form.cleaned_data.get("NativeSentence"),
            language = sp.native_language,
            author = request.user, 
            parent = None)
        target_sentence = Sentence.objects.create(
            text = form.cleaned_data.get("TargetSentence"),
            language = course.target_language,
            author = request.user, 
            parent = native_sentence)
        target_sentence.grammars.add(*GrammarsIncluded)
        if NativeFront:
            front, back = native_sentence, target_sentence
        else:
            front, back = target_sentence, native_sentence            
        # create question object
        question = Question.objects.create(
            front = front,
            back = back,
            hint = form.cleaned_data.get("Hint"),
            explanation = form.cleaned_data.get("Explanation"),
            author = request.user,
            target_language = course.target_language,
            native_language = sp.native_language,
        ) 
        messages.add_message(request, messages.SUCCESS, 'Question successfully saved')
        log = logging.getLogger(__name__)
        log.info("User" + request.user.username + "created question "+ str(question))
        # create a fresh form for the next entry
        form = CreateQuestionForm(course=course) 
            
    context = {
        'form': form,
        'selected_stages': selected_stages,
        'all_stages': all_stages,
        'course': course, 
        'native_language':sp.native_language,
        'target_language':course.target_language,
    }
    return render(request, 'Quiz/new_question.html', context)

@permission_required('quiz.content_creator',raise_exception=True)
def new_question_from_sentence(request):
    pass

@permission_required('quiz.content_creator',raise_exception=True)
def example_questions(request,grammar_id):
    grammar = get_object_or_404(Grammar, id = grammar_id)    
    student = get_object_or_404(Student, user = request.user) 
    sp = StudentProgress.objects.get(id=request.session['sp_pk'])
    native_languages = student.native_languages.values_list('id',flat=True)
    # find all sentences that contain this grammar
    example_sentence_ids = grammar.sentence_set.order_by().values_list('id',flat=True)
    # find all questions that are in the course's native language 
    example_sentences = grammar.sentence_set.all()
    front_related_questions = Question.objects.filter(front__in=example_sentence_ids).filter(back__language__in=native_languages)
    front_related_sentences = front_related_questions.values_list('front',flat=True)
    back_related_questions = Question.objects.filter(back__in=example_sentence_ids).filter(front__language__in=native_languages)  
    back_related_sentences = back_related_questions.values_list('back',flat=True)
    sentences_outside_questions = example_sentences.exclude(id__in=back_related_sentences).exclude(id__in=front_related_sentences)

    output={}
    counter=0
    # build list
    for question in front_related_questions:
            sentence_id = None
            native_sentence = question.back.text            
            if question.back.language != sp.native_language:
                sentence_id = question.front.id
#                native_sentence = "<Click to Create a matching translation>"    
            output[counter] = {'target_sentence':question.front.text,'native_sentence':native_sentence,'sentence_id':sentence_id,'question_id':question.id}
            counter += 1
    for question in back_related_questions:
            sentence_id = None
            native_sentence = question.front.text
            if question.front.language != sp.native_language:
                sentence_id = question.back.id
#                native_sentence = "<Click to Create a matching translation>"    
            output[counter] = {'target_sentence':question.back.text,'native_sentence':native_sentence,'sentence_id':sentence_id,'question_id':question.id}
            counter += 1
    for sentence in sentences_outside_questions:
            native_sentence = "<Click to Create a matching translation>"    
            output[counter] = {'target_sentence':sentence.text,'native_sentence':native_sentence,'sentence_id':sentence.id,'question_id':None}
            counter += 1

    context = {
        'output': output,
        'grammar': grammar,
        'grammar_title': grammar.full_title(sp.native_language)
    }
    return render(request, 'Quiz/example_questions.html', context)

    

@method_decorator(login_required, name='dispatch')
class StageAdd(CreateView):
    model = Stage
    fields = '__all__'
    success_url = reverse_lazy('Quiz:stage-list')

@method_decorator(login_required, name='dispatch')
class StageList(ListView):
    model = Stage
    
    def get_context_data(self, **kwargs):
        return ListView.get_context_data(self, **kwargs)
        
@method_decorator(login_required, name='dispatch')
class GramConceptCreate(CreateView):
    model = GramConcept
    fields = '__all__'
    success_url = reverse_lazy('Quiz:gramconcept-list')
        
@method_decorator(login_required, name='dispatch')
class GramConceptUpdate(UpdateView):
    model = GramConcept
    fields = '__all__'
    success_url = reverse_lazy('Quiz:gramconcept-list')

@method_decorator(login_required, name='dispatch')
class GramConceptDelete(DeleteView):
    model = GramConcept
    success_url = reverse_lazy('Quiz:dashboard')

@method_decorator(login_required, name='dispatch')
class GramConceptList(ListView):    
    model = GramConcept
    
    def get_context_data(self, **kwargs):
        return ListView.get_context_data(self, **kwargs)



class GrammarCreate(PermissionRequiredMixin,CreateView):
    permission_required = 'quiz.content_creator'
    model = Grammar
    template_name = 'Quiz/model_form.html'
    fields = '__all__'
    success_url = reverse_lazy('Quiz:dashboard')

    def get_context_data(self, **kwargs):
        context = super(GrammarCreate, self).get_context_data(**kwargs)
        context['model_name'] = 'Grammar'
        return context

class GrammarUpdate(PermissionRequiredMixin,UpdateView):
    permission_required = 'quiz.content_creator'
    model = Grammar
    template_name = 'Quiz/model_form.html'
    fields = '__all__'
    success_url = reverse_lazy('Quiz:dashboard')

    def get_context_data(self, **kwargs):
        context = super(GrammarUpdate, self).get_context_data(**kwargs)
        context['model_name'] = 'Grammar'
        return context

class GrammarDelete(PermissionRequiredMixin,DeleteView):
    permission_required = 'quiz.content_creator'
    model = Grammar
    template_name = 'Quiz/model_confirm_delete.html'
    success_url = reverse_lazy('Quiz:grammar-list')

    def get_context_data(self, **kwargs):
        context = super(GrammarDelete, self).get_context_data(**kwargs)
        context['model_name'] = 'Grammar'
        return context

@method_decorator(login_required, name='dispatch')
class GrammarList(ListView):    
    model = Grammar
    template_name = 'Quiz/model_list.html'
    
    def get_context_data(self, **kwargs):
        context = super(GrammarList, self).get_context_data(**kwargs)
        context['model_name'] = 'Grammar'
        context['create_url'] = 'Quiz:'+context['model_name'].lower()+'-create'
        context['update_url'] = 'Quiz:'+context['model_name'].lower()+'-update'
        context['delete_url'] = 'Quiz:'+context['model_name'].lower()+'-delete'
        return context
