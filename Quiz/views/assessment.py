from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.urls import reverse, reverse_lazy
from common import elo, css, massage
from ..models import * #GramConcept, Stage, Grammar, Student, StudentProgress, Course, Question, GrammarProgress
from datetime import date
from django.contrib import messages
import logging
import json
from django.template.loader import render_to_string
from django.http import HttpResponse


@login_required
def initial_placement_init(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + " starting initial placement")

    # set initial variables  
    request.session['mode'] = 'initial placement'
    request.session['asked qs'] = ''
    sp = get_object_or_404(StudentProgress, user=request.user, course=request.session['course_pk'])    
    return redirect('Quiz:run_initial_placement')

@login_required
def assessment_init(request):
    log = logging.getLogger(__name__)
    log.info("View assessment_init started")
    request.session['FOCUS_GOAL'] = 2 # quit once two focuses are located
    request.session['test_progress_fraction_current'] = -1/request.session['FOCUS_GOAL'] # used to calculate progress bar
    request.session['mode'] = 'weekly assessment'
    request.session.set_expiry(0) # session will expire when user browser is closed
    sp = get_object_or_404(StudentProgress, user=request.user, course=request.session['course_pk'])    
    sp.refresh_variance()
    if sp.variance > sp.VARIANCE_MIN_REQUIRED:
        sp.focuses = ""
        sp.save()
        return redirect('Quiz:assessment_schedule_qs')
    else:
        messages.add_message(request, messages.WARNING, "It is too early for another assessment. Please try again in a few days.")
        return redirect('Quiz:dashboard')        

@login_required
def assessment_schedule_qs(request):
    Q_SCHEDULE_GOAL = 5
    log = logging.getLogger(__name__)
    sp = get_object_or_404(StudentProgress, user=request.user, course=request.session['course_pk'])  
    if css.length(sp.focuses) >= request.session['FOCUS_GOAL']:
        return redirect('Quiz:assessment_report')
    questions_found = css.length(sp.scheduled_qs)

    for gp in GrammarProgress.objects.filter(sp=sp).order_by("grammar"):
        # Run some checks to possibly disqualify this gp as a question provider
        V=0.8 # V=0.8 => 56%...95%,     V=0.6 => 62%...96%
        pass_score = sp.stage.position - gp.grammar.stage.position + 1 / (sp.stage.position - gp.grammar.stage.position + 1 + V)  
        if gp.passing(sp.stage.position):
            continue # this gp has been successfully passed, move on
        if gp.recently_failed():
            continue         
        # this gp is approved as a question provider!
        sp.scheduled_qs = css.add_unique(sp.scheduled_qs,gp.select_question().id)
        sp.save()
        questions_found = css.length(sp.scheduled_qs)
        if questions_found >= Q_SCHEDULE_GOAL:
            break
    if questions_found > 0:
        return redirect('Quiz:assessment_ask_qs')
    else:
        new_stage = sp.promote()
        if new_stage is None:
            messages.add_message(request, messages.SUCCESS, 'Congratulations - you have graduated from all available stages for this course')
            log.info("User" + request.user.username + " graduated from all stages for " + str(sp.course))
            sp.initial_placement = False # aborts initial_assessment
            sp.save()
            return redirect('Quiz:dashboard')
        else:
            messages.add_message(request, messages.SUCCESS, 'Congratulations - you have graduated to Stage' + str(new_stage))
        return redirect('Quiz:assessment_schedule_qs')

@login_required
def assessment_report(request):        
    log = logging.getLogger(__name__)
    log.info("View assessment_report started")
    sp = get_object_or_404(StudentProgress, user=request.user, course=request.session['course_pk'])    
    focuses = 0
    output = "The grammars you should focus on studying for the coming week are:<ul>"
    for gp_id in css.to_list(sp.focuses):
        gp = GrammarProgress.objects.get(id=gp_id)
        focuses += 1
        output += "<li>"+gp.grammar.full_title(gp.sp.native_language)+"</li>"
    if focuses > 0:
        output +="</ul>"
        messages.add_message(request, messages.INFO, output)         
    return redirect('Quiz:dashboard')

@login_required
def assessment_ask_qs(request):
    sp = get_object_or_404(StudentProgress, user=request.user, course=request.session['course_pk'])    
    remaining_qs = css.to_list(sp.scheduled_qs)
    if len(remaining_qs) == 0:
        return redirect('Quiz:assessment_schedule_qs')
    else:            
        question = Question.objects.get(id=remaining_qs.pop())
        sp.scheduled_qs = css.to_css(remaining_qs)
        sp.save()
        testprogress  = css.length(sp.focuses)/request.session['FOCUS_GOAL'] + 1/request.session['FOCUS_GOAL'] + request.session['test_progress_fraction_current']
        return render(request, 'Quiz/question_ask.html', {'question': question, 'testprogress':testprogress})    
            
@login_required
def run_initial_placement(request):
    log = logging.getLogger(__name__)

    sp = get_object_or_404(StudentProgress, user=request.user, course=request.session['course_pk'])    
    # Stop the initial assessment once the user’s variance falls below a certain level.
    if sp.variance <= sp.VARIANCE_CUTOFF_INITIAL_PLACEMENT:
        messages.add_message(request, messages.SUCCESS, "Your initial assessment has been completed. You have reached stage " + str(sp.determine_stage()))
        if sp.determine_stage().position > 1:
            messages.add_message(request, messages.INFO, """Please note that the first weekly assessment you do will be 
                                                        longer than usual as we will take you quickly through previous 
                                                        stages to check for any gaps""") 
        sp.initial_placement = False # aborts initial_assessment
        sp.set_stage()
        log.info("User" + request.user.username + " initial placement completed. Placed at stage "+ str(sp.stage))
        messages.add_message(request, messages.INFO, "When you are ready, select WEEKLY ASSESSMENT to begin")
        return redirect('Quiz:dashboard')
    else:
        question = sp.select_nearest_ELO_q(css.to_list(request.session['asked qs']))
        if question is None:
            messages.warning(request, "No more questions can be found at your level. Initial placement will discontinue.")
            messages.add_message(request, messages.SUCCESS, "Your initial assessment has been completed. You have reached stage " + str(sp.determine_stage()))

            sp.initial_placement = False # aborts initial_assessment
            sp.set_stage() # also saves
            if sp.stage is None:
                log.warning("Course " + sp.course + " seems to have no stages registered")     
            elif sp.stage.position > 1:
                messages.add_message(request, messages.INFO, """Please note that the first weekly assessment you do will be 
                                                            longer than usual as we will take you quickly through previous 
                                                            stages to check for any gaps""") 
                messages.add_message(request, messages.INFO, "When you are ready, select WEEKLY ASSESSMENT to begin")
            return redirect('Quiz:dashboard')
        else:
            request.session['asked qs'] = css.add (request.session['asked qs'], question.id)
    testprogress = 1- (sp.variance - sp.VARIANCE_CUTOFF_INITIAL_PLACEMENT)/(sp.VARIANCE_MAX-sp.VARIANCE_CUTOFF_INITIAL_PLACEMENT)
    return render(request, 'Quiz/question_ask.html', {'question': question, 'testprogress':testprogress})    

@login_required
def ajax_submit_answer(request):
    # we don't want to have the question answers hidden, but visible if the html is inspected. Answers should be dynamically loaded.
    if request.is_ajax() and request.POST:
        question= get_object_or_404(Question, id=request.POST['question_id'])
        # massage the users entry
        simplified = massage.simplify(
            request.POST['entry'],
            native=question.native_language.iso693_2,
            target=question.target_language.iso693_2,
        )
        # look to see if an entry exists for the users entry
        # create or modify an entry
        matching_entries = Entry.objects.filter(question=question,simplified=simplified).all()
        if matching_entries.count() == 0:
            entry = Entry.objects.create(
                text=request.POST['entry'],
                simplified=simplified,
                question=question
            )
        else:
            entry = matching_entries.first()
            entry.increase()
            if matching_entries.count() > 1:                
                log.WARNING("Multiple Entries for same simplified value: Q.id:"+str(question.id)+" simplified:"+str(simplified))        
        # determine correctness of entered answer
        (eval,confident) = entry.score()
        examiner_comment = {(Entry.CORRECT,True):"That answer is correct",
                            (Entry.WRONG,True):"That answer is incorrect",
                            (Entry.REPORT,True):"The question is problematic. What do you think?",
                            (Entry.ALTERNATIVE,True):"That answer is different to what was desired, but a valid alternative",
                            (Entry.UNSCORED,False):"I'm not sure if that answer is correct or not, what do you think?",
                            (Entry.CORRECT,False):"That answer is probably correct. What do you think?",
                            (Entry.WRONG,False):"I think that answer may be incorrect. What do you think?",
                            (Entry.REPORT,False):"Hmm, the question was perhaps not very clear. What do you think?",
                            }
        # give user a chance to self-evaluate
        data = render_to_string('Quiz/post_answer_submit.html', 
                                {'question': question,
                                 'examiner_comment': examiner_comment[eval,confident],
                                 'examiner_result': eval,
                                 'evaluation_confident': confident,
                                 'Correct':Attempt.CORRECT, 
                                 'Wrong':Attempt.WRONG, 
                                 'Report':Attempt.REPORT,
                                 'raw_entry':request.POST['entry'],
                                 'entry_id':entry.id
                                 },
                                 request=request) 
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404

def question_score(request):
    # score the question_result. store results
    # received in form: question_id, question_result=CORRECT,WRONG, or FORGOTTEN
    question = get_object_or_404(Question, pk=request.POST['question_id'])
    course_pk = request.session['course_pk']
    sp = StudentProgress.objects.get(user=request.user,course=course_pk)
    studentELObefore = sp.score

    # create an attempt pointing to entry
    Attempt.objects.create(user=request.user,
                           raw_entry=request.POST['raw_entry'],
                           entry=Entry.objects.get(id=request.POST['entry_id']),
                           assessment=request.POST['self_assessment'],
                           feedback=request.POST['feedback'],
                           score=sp.score,
                           variance=sp.variance,
                           )
    
    grammar = question.grammar
    gp = GrammarProgress.objects.filter(sp=sp, grammar=grammar).first()
    if gp is None:
        if request.session['mode'] == 'weekly assessment':
            gp = GrammarProgress.objects.create_gp(sp=sp, grammar=grammar, stage_position=sp.stage.position)
        else:
            gp = GrammarProgress.objects.create(sp=sp, grammar=grammar)
    question_result = request.POST['self_assessment']
    GPpcbefore = gp.get_score()
    questionELObefore = question.score
    
    # calc new ELOs
    if question_result == Attempt.CORRECT:
        q_position = elo.LOSER
        s_position = elo.WINNER
        sp.variance = sp.variance * 0.95
    elif question_result == Attempt.REPORT:
        q_position = elo.DRAW
        s_position = elo.DRAW
        sp.variance = sp.variance * 0.9
    else: #Attempt.WRONG
        q_position = elo.WINNER
        s_position = elo.LOSER
        sp.variance = sp.variance * 0.8

    q_variance = question.variance / sp.variance # the greater the student's variance, the less impacted the question should be by that student's result    
    q_actor = elo.Actor(question.score, q_variance, q_position)
    s_actor = elo.Actor(sp.score, sp.variance, s_position)
    elo.determine_scores([q_actor,s_actor])
    gp.add_score(question_result)
    sp.rescore(s_actor.score)
    if request.session['mode'] == 'weekly assessment':
        if gp.recently_failed():
            # add as a focus
            sp.focuses = css.add_unique(sp.focuses,gp.id)
            sp.save()
            request.session['test_progress_fraction_current'] = -1/request.session['FOCUS_GOAL']
        else:
            request.session['test_progress_fraction_current'] *= 0.9 
        question.rescore(q_actor.score)
        if css.length(sp.scheduled_qs) == 0:
            return redirect('Quiz:assessment_schedule_qs')
        else:   
            return redirect('Quiz:assessment_ask_qs')
    elif request.session['mode'] == 'initial placement':  
        gp.delete()
        return redirect('Quiz:run_initial_placement')