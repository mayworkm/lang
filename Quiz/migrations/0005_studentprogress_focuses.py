# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-18 19:42
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import re


class Migration(migrations.Migration):

    dependencies = [
        ('Quiz', '0004_grammarprogress_started'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentprogress',
            name='focuses',
            field=models.TextField(blank=True, default='', null=True, validators=[django.core.validators.RegexValidator(re.compile('^\\d+(?:\\,\\d+)*\\Z', 32), code='invalid', message='Enter only digits separated by commas.')]),
        ),
    ]
