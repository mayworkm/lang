FROM python:2

WORKDIR /usr/src/app

# Setup dependencies
COPY Pipfile ./
RUN pip install pipenv
RUN pipenv install
RUN pipenv lock

# Install and run app
COPY . .
# Entrypoints allow for arguments to wsgi.py to be appended
# Change this to CMD if you just want an all-or-nothing
ENTRYPOINT python ./lang/wsgi.py
