mapping_dict = {}
mapping_dict['tur'] = {'ı':'i','İ':'I','ğ':'g','ç':'c','ş':'s','ö':'o','ü':'u'}
ignore_symbols = " ',.!?;:"

def simplify(input,native='eng',target='tur'):
    # native and target are iso693_2 codes
    # see https://www.loc.gov/standards/iso639-2/php/code_list.php
    if native.lower() in ("eng"):
        in_mapping='eng'
        if target.lower() in ("tur"):
            out_mapping='tur'        
            return map_string(input,in_mapping='eng',out_mapping='tur')
    else:
        return map_string(input)

def map_string(input,in_mapping='eng',out_mapping='tur'):
    simplified_string=''    
    for character in input:
        char = character.lower() 
        if char not in ignore_symbols:
            if char in mapping_dict[out_mapping]:
                simplified_string += mapping_dict[out_mapping][char]
            else:
                simplified_string += char    
    return simplified_string
    