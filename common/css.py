"""
    CSS: Comma Seperated String library
    Handles translation of strings of comma seperated integers into lists and back
"""

def to_list (css):
    # converts a css to list
    return [int(x) for x in css.split(',') if x.strip().isdigit()]

def to_css (list):
    # converts a list to css
    return ",".join(map(str, list))

def add (css, element):
    # add element to css string
    list = to_list(css)
    list.append(element)
    return to_css(list)

def add_unique (css, element):
    # Only add if does not already exist
    dict = {x: 1 for x in to_list(css)}
    dict[element] = 1
    return to_css(dict.keys())

def length (css):
    # returns number of elements
    return len(to_list(css))