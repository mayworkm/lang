# copy basic template from local settings file
from .settings import *

DATABASES = {
     'default': dj_database_url.config(conn_max_age=500),
}

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'y+*i+#023+#c*+xiq5pi&p=iz)&4hleuco+j5+qj%z^6i0!h#t'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = ['*']

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

