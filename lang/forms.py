from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from Quiz.models.language import Language

class StudentForm(forms.Form):
    native_languages = forms.ModelMultipleChoiceField(queryset=Language.objects.all(),help_text="Select all languages in which you are fluent")

    def clean_native_languages(self):
        data = self.cleaned_data['native_languages']
        if not data.exists():
            raise forms.ValidationError("You must select at least one language")
        return data
    